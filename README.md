# wx-mpvue-bluetooth

> A Mpvue 坑爹 project

## Build Setup

``` bash
# 初始化项目
vue init mpvue/mpvue-quickstart myproject
cd myproject

# 安装依赖
yarn

# 安装sass依赖
npm install -D node-sass sass-loader

# 开发时构建
npm dev

# 打包构建
npm build

# 指定平台的开发时构建(微信、百度、头条、支付宝)
npm dev:wx
npm dev:swan
npm dev:tt
npm dev:my

# 指定平台的打包构建
npm build:wx
npm build:swan
npm build:tt
npm build:my

# 生成 bundle 分析报告
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).



## 开启蓝牙

初始化蓝牙适配器（需要先调用此方法）

```js
wx.openBluetoothAdapter
```

| 错误码 | 错误信息             | 说明                                          |
| ------ | -------------------- | --------------------------------------------- |
| 0      | ok                   | 正常                                          |
| 10000  | not init             | 未初始化蓝牙适配器                            |
| 10001  | not available        | 当前蓝牙适配器不可用                          |
| 10002  | no device            | 没有找到指定设备                              |
| 10003  | connection fail      | 连接失败                                      |
| 10004  | no service           | 没有找到指定服务                              |
| 10005  | no characteristic    | 没有找到指定特征值                            |
| 10006  | no connection        | 当前连接已断开                                |
| 10007  | property not support | 当前特征值不支持此操作                        |
| 10008  | system error         | 其余所有系统上报的异常                        |
| 10009  | system not support   | Android 系统特有，系统版本低于 4.3 不支持 BLE |



## demo小例子

| demo         | desc                                                   |
| ------------ | ------------------------------------------------------ |
| 列表滑动删除 | @touchstart<br />@touchmove<br />@touchend<br />:style |
| 引导步骤     | 遮罩层定位                                             |
| 点击水波纹   | https://github.com/linrui1994/mpvue-ripple             |
| picker       | https://github.com/MPComponent/mpvue-picker            |



## mpvue坑

| 踩坑                                                         | 表现                                                 | 原因       | 解决                                                         | addr                                                  |
| ------------------------------------------------------------ | ---------------------------------------------------- | ---------- | ------------------------------------------------------------ | ----------------------------------------------------- |
| v-for遍历对象会遍历出__newReference                          | 如果遍历对象来生成页面结构会自动将该属性添加到页面上 | mpvue的bug | 升级到2.0.6版本，2.0.6版本将其设置为不可遍历                 | https://github.com/Meituan-Dianping/mpvue/issues/1455 |
| mpvue做表单时，在radio的change事件中修改data中的数据会导致radio点击无效，需要双击才能选中 | 数据实际上已更改，但是页面显示会表现为选不中radio    | 无解       | 既然改变不了data中数值，就将数据定义到created中，这算哪门子解决办法 | https://github.com/Meituan-Dianping/mpvue/issues/886  |
| picker选择器在选择后，会将form表单中的其他表单项置空，radio置为默认值 | 修改picker时其他表单项也跟着改变                     | 未知       | 慎用表单                                                     | ...                                                   |

