const mutations = {
  // 字体+
  fsAdd (state) {
    state.fs += 1;
  },
  // 字体-
  fsSub (state) {
    state.fs -= 1;
  },
  // 改变字体大小
  fsChange (state, val) {
    state.fs = val
    wx.setStorageSync('FS', val)
  }
}

export default mutations